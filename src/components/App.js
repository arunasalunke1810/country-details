import React from 'react';
import Home from '../Page/Home';
import '../Page/home.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import Countrydetails from '../Page/Countrydetails'
function App() {
    return (
        <Router>
            <Switch>
                <Route exact={true} path="/"  ><Home /></Route>
                <Route exact={true} path="/country-details/:name" component={Countrydetails} />

            </Switch>
        </Router>

    )
}
export default App;