import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import {
    Link
} from 'react-router-dom'

function Countrydetails() {
    const [country, setCountry] = useState({});
    const [loader, setLoader] = useState(false)

    let { name } = useParams();
    const getCountrydetail = () => {
        setLoader(true);
        axios.get("https://restcountries.eu/rest/v2/name/" + name + "?fullText=true")
            .then((response) => {
                setCountry(response.data[0])
                setLoader(false);
            })
            .catch((error) => {
                console.log(error)
                setLoader(false);
            })
    }
    useEffect(() => {

        getCountrydetail();

    }, [])

    return (
        <div> {loader ? <div className="loader">Loading {name} details, Please wait...</div> : <div className="container">

            <div className="imgcon">
                <img className="image" src={country.flag} />
            </div>
            <Link className="back" to="/">Back</Link>
            <div className="data">
                <div className="countryname1">
                    Name: <b>{country.name}</b>
                </div>

                <div className="countrycode">
                    Country Codes: <b>{country.alpha2Code}, {country.alpha3Code}</b>
                </div>
                <div className="countrycurrencies">
                    Currencies:
                    <ol>
                        {country.currencies && country.currencies.map((data, i) => (
                            data.name && (<li key={i}>
                                <b>{data.name} - {data.code} ({data.symbol})</b>

                            </li>)
                        ))}
                    </ol>
                </div>
                <div className="borders">
                    Borders:
                    <ol>
                        {country.borders && country.borders.map((data1, i) => (
                            <li key={i}><b>{data1}</b> </li>
                        ))}

                    </ol>
                    <div className="callingcodes">
                        CallingCodes:
                        <ol>
                            {country.callingCodes && country.callingCodes.map((data2, i) =>
                                <li key={i}>
                                    <b>{data2}</b>
                                </li>
                            )}
                        </ol>
                    </div>
                    <div className="capital">
                        Capital: <b>{country.capital}</b>
                    </div>
                    <div className="population">
                        Population: <b>{country.population}</b>
                    </div>
                    <div className="area">
                        Area: <b>{country.area}SqKm</b>
                    </div>
                    <div className="nativename">
                        NativeName: <b>{country.nativeName}</b>
                    </div>
                    <div className="numericcode">
                        NumericCode: <b>{country.numericCode}</b>
                    </div>
                    <div className="region">
                        Region: <b>{country.region}</b>
                    </div>
                    <div className="languages">
                        Languages:
                        <ol>
                            {country.languages && country.languages.map((data3, i) =>
                                <div key={i}>
                                    <li> <b>{data3.name}({data3.iso639_1}, {data3.iso639_2})-{data3.nativeName}</b></li>
                                </div>
                            )}
                        </ol>
                    </div>
                    <div className="latlng">
                        LatLng: {country.latlng && (<b>{country.latlng[0]}, {country.latlng[1]}</b>)}
                    </div>
                </div>
            </div>

        </div>}

        </div>

    )
}
export default Countrydetails;