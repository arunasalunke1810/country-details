import axios from 'axios';
import React, { useEffect, useState } from 'react';

import {
    Link
} from 'react-router-dom';

function Home() {
    const [countries, setCountries] = useState([]);
    const [loading, setLoading] = useState(false);
    const [orignalcountries, setOrignalCountries] = useState([]);
    const getAllCountries = () => {
        setLoading(true);
        axios.get("https://restcountries.eu/rest/v2/all")
            .then((response) => {
                setCountries(response.data);
                setOrignalCountries(response.data);
                setLoading(false);

            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    }
    const filter = (e) => {
        let searchKey = e.currentTarget.value.trim();
        if (searchKey) {
            let filteredCountries = orignalcountries.filter((item, key) => {
                return item.name.toLowerCase().includes(searchKey.toLowerCase())
            });
            setCountries(filteredCountries);
        }
        else {
            setCountries(orignalcountries);
        }

    }
    useEffect(() => {
        getAllCountries();

    }, [])

    return (
        <div>
            {loading ? <div className="loader">Loading Country List, Please wait...</div> : <div className="container">
                <div className="name" >Countries</div>
                <input className="searchbar" type="text" placeholder="Type here to search" onInput={filter} />
                {countries.length > 0 ? (<div>
                    {countries.map((singledata, i) =>
                        <div key={i} className="countryname">
                            <div className="countryN">
                                <Link className="link" to={"/country-details/" + singledata.name}> {singledata.name}({singledata.alpha2Code})</Link>
                            </div>
                        </div>
                    )}
                </div>) : (<div className="errormessage">Country Not Found!</div>)}
            </div>
            }

        </div>
    )
}
export default Home;